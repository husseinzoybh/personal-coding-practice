# Scratch Codes

Starting off:

1. To Start with make a backdrop.
2. Next you want to choose 2 alternate colors and make small sexctions on the length of the board.
3. Make a center divider line use shift to make it straight.
4. Find sprites, youll need a bar, as well as a ball.

Coding the Paddles:

1. Get a “When clicked” Block from the control section
2. Use a “go to” block and set it to x = -227 y = 0 (positions the paddle to the middle of the map)
3. “Point in direction” block set to 0 (Controls the direction the paddle is pointed)
4. Grab  a “Forever” Block
5. Grab an “If Then” block and a “Key pressed” block
6. With the key pressed block inserted into the if block wet the options to W
7. Set change by to 10 (manages the speed of the paddle)
8. Repeat steps 5-7 and set the block to S and -10
9. Duplicate the paddle block 
10. Set “go to” x =227 y = 0
11. Change the input to up arror and second one to down arrow

Coding the Ball :

1. Grab a “when clicked” block
2. Grab 2 “Set score” blocks and send them to 0 with the names Left score and Right score
3. Grab a “Set size” block and set it to 50%
4. Grab a “Go to” block and set the x and y to 0 so the ball starts at the middle of the map
5. Grab a “Point in direction” block and make it 95 ( throws of the trajectory of the ball so its not straight)
6. Grab a “Forever” blocks 
7. Within the forever block add the “move” bloack and set it to 10
8. Add the “if on edge block” ( So the block will bounce when it touches the paddle.)

Coding the Ball cont.:

1. Grab a “when clicked” block
2. Grab a “Forever” block
3. Grab 4 “if then” blocks
4. Grab 4 “touching color” Blocks
5. Grab 2 “point in direction” 2 “not” and 2 “touching” blocks
6. Grab 2 “change” blocks and label them left and right score and set it to 1
7. Use the dripper to copy the color of your 2 sides and add them to the both to 2 touching color blocks
8. Set the blocks wit this knowledge. if the ball touches a certain color change the score by 1 wait until the ball stop touching that color to score again.
9. Set the blocks with this knowledge. if the ball touches the paddle then point the ball in the direction of 170 and wait until the ball stops touching the paddle to do it again.