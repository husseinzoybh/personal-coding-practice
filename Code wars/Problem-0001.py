# data given
# write a program from 1-100


# data i have to manipulate
# # for multiple of 3 print fizz
# # for multiple of 5 print buzz
# # for multiples of 3,5 print fizzbuzz


# how am i returning data

# # multiple print statments for multiples of 3 ,5 and combined and my iterated numbers 1-100


length = 100

for number in range(1,length +1) :
    if number % 3 == 0 and number % 5 == 0:
        print ("FizzBuzz")
    elif number % 3 == 0:
        print ("Fizz")
    elif number % 5 == 0:
        print ('Buzz')
    
    else:
        print(number)

# Need to finish this faster